package model.persistence;

import controller.User;

public interface IUserPersistence {

	User readUser(String username);

}